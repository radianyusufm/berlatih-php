<?php


function ubah_huruf($string){

  $huruf = ['a','b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
             'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

  $array_inputan = [];
  $hasil_inputan_array = [];

  for ($i=0; $i < strlen($string) ; $i++) {
    array_push($array_inputan, $string[$i]);
  }

  for ($j=0; $j < count($array_inputan); $j++) {
    $key = array_search($array_inputan[$j], $huruf);
    $key = $key + 1;
    $hasil = $huruf[$key];
    array_push($hasil_inputan_array, $hasil);
  }

  $hasil_akhir = implode("",$hasil_inputan_array);
  return $hasil_akhir;

}


// TEST CASES
echo ubah_huruf('wow'); // xpx
echo "<br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br>";
echo ubah_huruf('keren'); // lfsfo
echo "<br>";
echo ubah_huruf('semangat'); // tfnbohbu

?>
